﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace projektas
{
    public partial class Form1 : Form
    {
        int a=0, b=0;
        int ans;
        bool notCalculated;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            notCalculated = true;
        }

        private void btnOne_Click(object sender, EventArgs e)
        {
            textBox1.Text += "1";
        }

        private void btnTwo_Click(object sender, EventArgs e)
        {
            textBox1.Text += "2";
        }

        private void btnThree_Click(object sender, EventArgs e)
        {
            textBox1.Text += "3";
        }

        private void btnFour_Click(object sender, EventArgs e)
        {
            textBox1.Text += "4";
        }

        private void btnFive_Click(object sender, EventArgs e)
        {
            textBox1.Text += "5";
        }

        private void btnSix_Click(object sender, EventArgs e)
        {
            textBox1.Text += "6";
        }

        private void btnSeven_Click(object sender, EventArgs e)
        {
            textBox1.Text += "7";
        }

        private void btnEight_Click(object sender, EventArgs e)
        {
            textBox1.Text += "8";
        }

        private void btnNine_Click(object sender, EventArgs e)
        {
            textBox1.Text += "9";
        }

        private void btnCE_Click(object sender, EventArgs e)
        {
            a = 0;
            b = 0;
            ans = 0;
            textBox1.Text = "";
        }

        private void btnEqual_Click(object sender, EventArgs e)
        {
            textBox1.Text = ans.ToString();
        }

        private void btnPlus_Click(object sender, EventArgs e)
        {
            if (notCalculated)
            {
                a = Convert.ToInt32(textBox1.Text);
                textBox1.Text = "";
            }
            b = Convert.ToInt32(textBox1.Text);
            notCalculated = true;
            ans = a + b;
            label1.Text = a.ToString();
            label2.Text = b.ToString();
        }
    }
}
